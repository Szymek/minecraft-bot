﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_Bot
{
    class CompressedPacket : Packet
    {
        public CompressedPacket(Packets p) : base(p) { }
        public override byte[] Get()
        {
            byte[] uncompressed;
            byte[] compressed;
            using (var cMs = new MemoryStream())
            {
                var data = this.ms.ToArray();
                using (var bw = new BinaryWriter(cMs))
                {
                    Packet.WriteNumber(bw, this.ID);
                    bw.Write(data);
                }
                //cMs.Write(data, 0, data.Length);
                uncompressed = cMs.ToArray();
#if DEBUG
                Console.WriteLine("UC: " + Encoding.UTF8.GetString(uncompressed));
#endif
                //compressed = this.Compress(uncompressed);
                //Console.WriteLine("cC: " + Encoding.UTF8.GetString(compressed));
            }


            byte[] header;
            using (var hMs = new MemoryStream())
            {
                using (var hBw = new BinaryWriter(hMs))
                {
                    Packet.WriteNumber(hBw, uncompressed.Length + 1);
                    Packet.WriteNumber(hBw, 0);
                }
                header = hMs.ToArray();
            }
            byte[] pakiet;
            using (var fMs = new MemoryStream())
            {
                fMs.Write(header, 0, header.Length);
                fMs.Write(uncompressed, 0, uncompressed.Length);
                pakiet = fMs.ToArray();
            }
#if DEBUG
            Console.WriteLine("-------------------------\nWysylany");
            Console.WriteLine("Pakiet: {0}", BitConverter.ToString(pakiet).Replace("-", " ").ToUpper());
            Console.WriteLine("Dlugosc bez kompresji: {0}", uncompressed.Length);
            //Console.WriteLine("Dlugosc z kompresja: {0}", compressed.Length);
            Console.WriteLine("-----------------------------");
#endif
            return pakiet;
        }
        private byte[] Compress(byte[] b)
        {
            using (var cMs = new MemoryStream())
            {
                using (var ds = new DeflateStream(cMs, CompressionMode.Compress))
                {
                    ds.Write(b, 0, b.Length);
                }
                return cMs.ToArray();
            }
        }
    }
}
