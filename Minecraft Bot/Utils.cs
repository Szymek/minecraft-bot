﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_Bot
{
    public static class Utils
    {


        public static string GenerateRandomString(int l = 15)
        {
            return Guid.NewGuid().ToString().Replace("-", "").Substring(0, l);
        }

        public class MCReader : BinaryReader
        {
            public MCReader(Stream s) : base(s) { }
            public int ReadPacketLength()
            {
                uint r = 0;
                int l = 0;
                byte c;
                while (true)
                {
                    c = base.ReadByte();
                    r |= (c & 0x7Fu) << l++ * 7;
                    if ((c & 0x80) != 128)
                        break;
                }
                return (int)r;

            }
        }
    }
}
