﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_Bot
{
    public class Minecraft
    {
        public bool Connected {
        
            get
            {
                return this.Socket != null && this.Socket.Connected;
            }
        }
        public bool IsCompressionEnabled { get; set; }
        public string IP { get; set; }
        public ushort Port { get; set; }
        public string Username { get; set; }
        public Socket Socket { get; set; }

        public Minecraft(string ip, int port, string nick)
        {
            this.IP = ip;
            this.Port = (ushort)port;
            this.Username = nick;
        }

        public Minecraft(string ip, int port) : this(ip, port, Utils.GenerateRandomString()) { }
        public void Connect()
        {
            if (Connected)
                return;
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.Socket.Connect(this.IP, this.Port);
            Task.Factory.StartNew(socketThread);

            //handshake
            using (var p = new Packet(Packets.Handshake))
            {
                p.Write((byte)107);
                p.Write(this.IP);
                p.Write((ushort)25565);
                p.Write((byte)2);
                this.SendPacket(p);
            }
            /*//request
            using (var p = new Packet(Packets.Request))
            {
                this.SendPacket(p);
            }*/
            //logowanie
            using (var p = new Packet(Packets.LoginStart))
            {
                p.Write(this.Username);
                this.SendPacket(p);
            }

        }

        public void SendChatMessage(string msg)
        {
            using (var p = new CompressedPacket(Packets.ChatMessage))
            {
                p.Write(msg);
                this.SendPacket(p);
            }
        }

        private void socketThread()
        {
            byte[] buffer;
            int length;
            while (true)
            {
                buffer = new byte[4096];
                length = this.Socket.Receive(buffer, 0, buffer.Length, SocketFlags.None);
                if (length > 0)
                {
                    HandlePacket(buffer.Take(length).ToArray());
                }
            }
        }
        private int pCount = 0;
        private void HandlePacket(byte[] packet)
        {
            byte pID;
            if (packet[1] == 0)
                pID = packet[2];
            else
                pID = packet[1];
#if DEBUG
            Console.WriteLine("PID: {0}", pID.ToString("X2"));
            //Console.WriteLine("Pakiet odebrany: " + BitConverter.ToString(packet).Replace("-", " ").ToUpper());
#endif
            if (pID == 0)
            {
                if (OnDisconnect != null)
                    OnDisconnect(Encoding.UTF8.GetString(packet));
            }
            else if (pCount < 5 && pID == 3)
            {
                this.IsCompressionEnabled = true;
                if (OnSpawn != null)
                    OnSpawn();
            }
            pCount++;
        }
        public void SendPacket(Packet p)
        {
            this.SendPacket(p.Get());
        }
        public void SendPacket(byte[] buffer)
        {
            try {
                this.Socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
            }
            catch (Exception ex) { Console.WriteLine("Połączenie zerwane! ({0})", ex); }
        }

        public delegate void DisconnectHandler(string reason);
        public event DisconnectHandler OnDisconnect;
        public delegate void SpawnHandler();
        public event SpawnHandler OnSpawn;
    }
}
