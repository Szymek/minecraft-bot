﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_Bot
{
    public class Packet : IDisposable
    {
        public byte ID { get; set; }
        protected MemoryStream ms { get; set; }
        protected BinaryWriter bw { get; set; }

        public Packet(byte id)
        {
            this.ID = id;
            this.ms = new MemoryStream();
            this.bw = new BinaryWriter(ms);
        }
        public Packet(Packets p) : this((byte)p) { }

        public void Dispose()
        {
            bw.Dispose();
            ms.Dispose();
        }
        public virtual byte[] Get()
        {
            var data = this.ms.ToArray();
            byte[] header;
            using (var hMs = new MemoryStream())
            {
                using (var hBw = new BinaryWriter(hMs))
                {
                    //dlugosc
                    WriteNumber(hBw, data.Length + 1);
                    //id pakietu
                    hBw.Write(this.ID);
                }
                header = hMs.ToArray();
            }
#if DEBUG
                Console.WriteLine("-------------------------\nWysylany");
                Console.WriteLine("Pakiet: 0x{0}", this.ID.ToString("X"));
                Console.WriteLine("Header: 0x{0}", BitConverter.ToString(header).Replace("-", " ").ToUpper());
                Console.WriteLine("Data: 0x{0}", BitConverter.ToString(data).Replace("-", " ").ToUpper());
                Console.WriteLine("-----------------------------");
#endif
            using (var fMs = new MemoryStream())
            {
                fMs.Write(header, 0, header.Length);
                fMs.Write(data, 0, data.Length);
                return fMs.ToArray();
            }
        }


        public void Write(int v)
        {
            this.bw.Write(v);
        }
        public void Write(string v)
        {
            this.bw.Write(v);
        }
        public void Write(ushort v)
        {
            this.bw.Write(Reverse(v));
        }
        public void Write(byte v)
        {
            this.bw.Write(v);
        }
        public static void WriteNumber(BinaryWriter bw, int n)
        {
            if (n < byte.MaxValue)
                bw.Write((byte)n);
            else if (n < ushort.MaxValue)
                bw.Write(Reverse((ushort)n));
            else
                bw.Write(n);
        }
        public static ushort Reverse(ushort v)
        {
            return (ushort)((v & 0xFFU) << 8 | (v & 0xFF00U) >> 8);
        }
    }
}
