﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_Bot
{
    public enum Packets
    {
        Handshake = 0x00,
        Request = 0x00,
        LoginStart = 0x00,
        Disconnect = 0x00,
        Spawn = 0x02,
        ChatMessage = 0x02
    }
}
