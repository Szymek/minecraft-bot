﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Minecraft_Bot
{
    public class Program
    {

        static void Main(string[] args)
        {
#if DEBUG            
            if (BitConverter.IsLittleEndian)
                Console.WriteLine("Little endian!");
#endif
            string ip;
            int port;
            Console.WriteLine("Podaj IP serwera");
            ip = Console.ReadLine();
            if (string.IsNullOrEmpty(ip))
                ip = "127.0.0.1";
            Console.WriteLine("Podaj port serwera (lub zostaw puste, jeżeli domyślny)");
            if (!int.TryParse(Console.ReadLine(), out port))
                port = 25565;
            Console.WriteLine("Testowanie połączenia...");
            if (tryToConnect(ip, port))
            {
                Console.WriteLine("Suckes!");
                Console.WriteLine("Podaj ilość wątków");
                short iloscWatkow;
                while (!short.TryParse(Console.ReadLine(), out iloscWatkow) || iloscWatkow <= 0)                
                    Console.WriteLine("To nie wygląda na poprawną liczbę...");
                Console.WriteLine("Podaj wiadomość do wysyłania");
                string msg = Console.ReadLine();
                for (var i = 0; i < iloscWatkow; i++)
                    Task.Factory.StartNew(() => minecraftThread(ip, port, msg));
                while (true) { System.Threading.Thread.Sleep(1500); }
            }
            else
            {
                Console.WriteLine("Wystąpił błąd w trakcie połączenia.\nNa pewno chodziło o {0}:{1}?", ip, port);
                Console.ReadKey();
            }
        }
        private static bool tryToConnect(string ip, int port)
        {
            using (var s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                try
                {
                    s.Connect(ip, port);
                    s.Disconnect(false);
                    return true;
                }
                catch (Exception)
                { }
            }
            return false;
        }
        private static Random random = new Random();
        private static int _n = 0, _m = 0;
        public static void minecraftThread(string ip, int port, string msg)
        {
            try
            {
                var mc = new Minecraft(ip, port);
                mc.OnDisconnect += (reason) =>
                {
                    Console.WriteLine("Rozłączono! ({0})", reason);
                };
                mc.OnSpawn += () =>
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Bot #{0} dołączył do serwera", ++_n);
                    while (mc.Connected)
                    {
                        System.Threading.Thread.Sleep(random.Next(1500, 3500));
                        mc.SendChatMessage(msg);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Ilość wysłanych wiadomości: {0}", ++_m);
                    }
                };
                mc.Connect();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Blad: {0}", ex);
            }
        }
    }
}
